from ..app.model.rss_model import RSS
import pytest


@pytest.fixture(scope='module')
def new_rss():
    rss = RSS(5, 'name1', "description1", "https://example.com")
    return rss