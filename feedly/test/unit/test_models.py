def test_new_rss_with_fixture(new_rss):
    assert new_rss.rss_id == 5
    assert new_rss.name == 'name1'
    assert new_rss.description == 'description1'
    assert new_rss.url == "https://example.com"