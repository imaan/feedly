from .. import db


class RSS(db.Model):
    # __tablename__ = "table_rss_01"
 
    id = db.Column(db.Integer, primary_key=True)
    rss_id = db.Column(db.Integer(), unique = True)
    name = db.Column(db.String())
    description = db.Column(db.String(120))
    url = db.Column(db.String(120))
 
    def __init__(self, rss_id=None, name=None, description=None, url=None):
        self.rss_id = rss_id
        self.name = name
        self.description = description
        self.url = url
 
    def __repr__(self):
        return '<RSS %r>' % self.rss_id