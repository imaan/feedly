import feedparser
import asyncio
import platform

async def fetch(url):
        return feedparser.parse(url)

async def fetch_all(urls):
    tasks = []
    for url in urls:
        task = asyncio.create_task(fetch(url))
        tasks.append(task)
    results = await asyncio.gather(*tasks)
    return results

async def scrape(urls):    
    feeds = await fetch_all(urls)
    fetched_feeds = []
    [fetched_feeds.extend(feed["items"]) for feed in feeds] #aggregate feeds
    sorted_fetched_feeds = sorted(fetched_feeds, key=lambda entry: entry["published"])
    sorted_fetched_feeds.reverse()
    return sorted_fetched_feeds

def run_scrape(urls):
    if platform.system() == "Windows":  #to avoid asyncio bug on windows
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    return asyncio.run(scrape(urls))