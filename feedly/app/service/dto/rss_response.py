from ...model.rss_model import RSS

def to_response(rss):
	return {
			"id": rss.id,
			"rss_id": rss.rss_id,
			"name": rss.name,
			"description": rss.description,
			"url": rss.url
	}