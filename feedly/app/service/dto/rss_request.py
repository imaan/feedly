from ...model.rss_model import RSS

def to_entity(request):
	rss = RSS()
	rss.name = request.form.get('name')
	rss.description = request.form.get('description')
	rss.url = request.form.get('url')
	return rss