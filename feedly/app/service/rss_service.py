from markupsafe import escape
from ..model.rss_model import RSS
from .dto.rss_response import to_response
from .dto.rss_request import to_entity
from . import rss_scraper
from .. import db 
import uuid
import json
from flask import jsonify
import asyncio


class RssService():
    def __init__(self):
        pass

    def find_all(self):
        rsss = RSS.query.all()         
        return jsonify([to_response(rss) for rss in rsss])

    def find_by_id(self, id):
        rss = RSS.query.filter_by(id=id).first()
        return to_response(rss)

    def create(self, request):
        rss = to_entity(request)
        rss.rss_id = str(uuid.uuid4())
        self.save_changes(rss)
        return "created {0}".format(rss.id)

    def delete(self, id):
        if not id or id != 0:
            rss = RSS.query.get(id)
            if rss:
                db.session.delete(rss)
                db.session.commit()
            return "success"
        return "Please enter a valid id"

    def fetch_feeds(self):
        rsss = RSS.query.all()
        urls = []            
        [urls.append(rss.url) for rss in rsss]
        result = rss_scraper.run_scrape(urls)
        return jsonify(result)

    def save_changes(self, rss: RSS) -> None:
        db.session.add(rss)
        db.session.commit()