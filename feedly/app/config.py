import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY='dev'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app_main.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False