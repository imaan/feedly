from flask import Flask, current_app

from .config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

db = SQLAlchemy()

def create_app(test_config=None) -> Flask:
	app = Flask(__name__)
	app.config.from_object(Config)
	from .model.rss_model import RSS
	db.init_app(app)
	with app.app_context():
		db.create_all()
	from . import rss
	app.register_blueprint(rss.bp)
	return app