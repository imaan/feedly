from flask import Flask, request, Blueprint
from markupsafe import escape
from flask import url_for

from . import db
from .service.rss_service import RssService
from .model.rss_model import RSS

bp = Blueprint('rss', __name__, url_prefix='/rss')

service = RssService()

@bp.route("/find-all/", methods=['GET'])
def find_all():
    return service.find_all()

@bp.route('/<int:id>', methods=['GET'])
def find_by_id(id):
    return service.find_by_id(id)

@bp.route('/', methods=['POST'])
def create():
     return service.create(request)

@bp.route('/<int:id>', methods=['DELETE'])
def delete(id):
     return service.delete(id)

@bp.route('/feeds/', methods=['GET'])
def fetch_feeds():
    return service.fetch_feeds()